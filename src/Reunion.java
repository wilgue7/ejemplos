/**
 * Created by Wilmer on 08/05/2017.
 */
import java.util.Calendar;
public class Reunion {
    /**
     * Si por algún motivo requerimos cualquier tipo de computación para inicializar nuestras variables estáticas,
     * utilizaremos lo que se conoce como  bloque estático o inicializador estático,
     * el cuál se ejecuta una sola vez, cuando se carga la clase.
     */
    static {
        int zona_horaria = Calendar.getInstance().get(Calendar.ZONE_OFFSET)
                / (60 * 60 * 1000);
    }
}
