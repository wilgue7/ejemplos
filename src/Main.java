public class Main {

    public static void main(String[] args) {

        Usuario raul = new Usuario();
        Usuario juan = new Usuario();
        /**Podemos acceder a estos métodos y atributos bien desde la propia clase
         *
         */
        System.out.println("Hay " + Usuario.usuarios + " usuarios");
        /**o bien desde una instancia cualquiera de la clase:
         *
         */
        System.out.println("Hay " + raul.usuarios + " usuarios");

        System.out.println("Hello World!");
    }




}
