/**
 * Created by Wilmer on 08/05/2017.
 */
public class EjemploFinal {
    /**
     * Si una variable se marca como final, no se podrá asignar un nuevo valor a la variable. Si una clase se marca como final,
     * no se podrá extender la clase. Si es un método el que se declara como final, no se podrá sobreescribir.
     *
     */
    public static void main(String[] args) {
        final String cadena = "Hola";

        /*
        cadena = new String("Adios");
        */
    }
}
